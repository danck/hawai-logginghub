package logginghub

import (
	"bytes"
	"log"
	"net/http"
	"sync"
)

var (
	subscribe   chan *Message
	unsubscribe chan *Message
	publish     chan *Message
)

func startHub() {
	subscribe = make(chan *Message, 32)
	unsubscribe = make(chan *Message, 32)
	publish = make(chan *Message, 4096)
	go subscriber()
	go publisher()
}

// Mutex-protected set of subscribers (map[<channel>][<subscribers>]int)
var subscribers = struct {
	sync.RWMutex
	byChannel map[string]map[string]int
}{byChannel: make(map[string]map[string]int)}

// subscriber adds and removes subscribers
func subscriber() {
	defer log.Fatal("Subscriber crashed")
	for {
		select {
		case msg := <-subscribe:
			subscribers.Lock()
			subs, ok := subscribers.byChannel[msg.Channel]
			if !ok {
				subs = make(map[string]int)
				subscribers.byChannel[msg.Channel] = subs
			}
			subscribers.byChannel[msg.Channel][string(msg.Content[:])] = 0 // arbitrary value
			subscribers.Unlock()
			log.Println("New subscriber:", string(msg.Content[:]))
		case msg := <-unsubscribe:
			subscribers.Lock()
			delete(subscribers.byChannel[msg.Channel], string(msg.Content[:]))
			subscribers.Unlock()
			log.Println("Subscriber removed:", string(msg.Content[:]))
		}
	}
}

// publisher pumps messages from on a channel to all subscribers
func publisher() {
	defer log.Fatal("Publisher crashed")
	var wg sync.WaitGroup
	for {
		msg := <-publish
		log.Println("Publishing message")
		subscribers.RLock()
		log.Println("acquired lock")
		log.Printf("Channel: %s, Content %s", msg.Channel, string(msg.Content))

		// loop over subscribers and concurrently publish messages
		for sub := range subscribers.byChannel[msg.Channel] {
			wg.Add(1)
			go func() {
				log.Println("Publishing message to", sub)
				defer wg.Done()
				resp, err := http.Post(
					sub,
					"application/text",
					bytes.NewReader(msg.Content))
				if err != nil {
					log.Printf("Error while publishing to %s: %s", sub, err.Error())
					return
				}
				defer resp.Body.Close()
			}()

		}
		wg.Wait()
		subscribers.RUnlock()
		log.Println("done publishing")
	}
}
