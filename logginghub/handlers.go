package logginghub

import (
	"github.com/gorilla/mux"
	"io/ioutil"
	"log"
	"net/http"
)

// pubHandler receives lines of text
func pubHandler(w http.ResponseWriter, r *http.Request) error {
	log.Print("Hit pub handler")
	if r.Method != "POST" {
		return NotImplemented{"HTTP Verb not supported"}
	}
	vars := mux.Vars(r)
	channel := vars["channel"]
	if channel == "" {
		return BadRequest{"No channel specified"}
	}
	data, err := ioutil.ReadAll(r.Body)
	log.Println("Received:", string(data))
	if err != nil {
		return err
	}
	defer r.Body.Close()
	publish <- &Message{
		Channel: channel,
		Content: data,
	}
	return nil
}

// subHandler registers subscibers
func subHandler(w http.ResponseWriter, r *http.Request) error {
	log.Print("Hit sub handler")
	vars := mux.Vars(r)
	channel := vars["channel"]
	if channel == "" {
		return BadRequest{"No channel specified"}
	}
	subscriberAddress, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return err
	}
	defer r.Body.Close()
	if r.Method == "POST" {
		log.Print("Post called")
		subscribe <- &Message{
			Content: subscriberAddress,
			Channel: channel,
		}
		return nil
	}
	if r.Method == "DELETE" {
		unsubscribe <- &Message{
			Content: subscriberAddress,
			Channel: channel,
		}
		return nil
	}
	return NotImplemented{"HTTP Verb not supported"}
}
