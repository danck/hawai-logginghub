package logginghub

type Message struct {
	Channel string `json:"channel"`
	Content []byte `json:"content"`
}
