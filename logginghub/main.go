package logginghub

import (
	"flag"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"os"
)

var (
	listenAddress = flag.String(
		"listen-addr",
		":20000",
		"Address to listen for incoming log posts")
)

func Main() {
	flag.Parse()

	// Set up logger
	lf := os.Getenv("LOG_FILE")
	if lf == "" {
		lf = "hawai-logginghub.log"
	}

	f, err := os.OpenFile(lf, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening log file: %v", err)
	}
	defer f.Close()
	//log.SetOutput(f)

	startHub()

	router := mux.NewRouter()
	router.HandleFunc("/pub/{channel:[0-9a-zA-Z]+}", errorHandler(pubHandler))
	router.HandleFunc("/sub/{channel:[0-9a-zA-Z]+}", errorHandler(subHandler))

	log.Printf("Starting to listen on %s", *listenAddress)
	log.Fatal(http.ListenAndServe(*listenAddress, router))
}
