FROM golang

# Set up the executables
ADD . /go/src/gitlab.com/danck/hawai-logginghub
RUN go get github.com/gorilla/mux
RUN go install github.com/gorilla/mux
RUN go install gitlab.com/danck/hawai-logginghub
RUN go get gitlab.com/danck/hawai-runner.git
RUN go install gitlab.com/danck/hawai-runner

# Evironment for the runner
ENV PROTOCOL_SCHME=http
ENV EXTERNAL_HOST_ADDRESS=192.168.29.143
ENV EXTERNAL_HOST_PORT=20000
ENV LOG_FILE=hawai-logginghub.log
ENV SERVICE_COMMAND="/go/bin/hawai-logginghub"
ENV SERVICE_LABEL=logging
ENV REGISTRY_ADDRESS='http://192.168.29.143:32000/service'

ENTRYPOINT /go/bin/hawai-runner

EXPOSE 20000 

